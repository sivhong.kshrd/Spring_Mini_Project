package com.example.demo.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Book {

    @NotNull(message = "please enter id")
    private Integer id;

    @Size(min=1,max = 225,message = "valid 1 to 255")
    private String title;

    @NotEmpty(message = "please enter author")
    private String author;

    @NotEmpty(message = "please enter publisher")
    private String publisher;
    private String thumbnail;

    private Category category;

    public Book(){}

    public Book(@NotNull(message = "please enter id") Integer id, @Size(min = 1, max = 225, message = "valid 1 to 255") String title, @NotEmpty(message = "please enter author") String author, @NotEmpty(message = "please enter publisher") String publisher, String thumbnail, Category category) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.thumbnail = thumbnail;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
