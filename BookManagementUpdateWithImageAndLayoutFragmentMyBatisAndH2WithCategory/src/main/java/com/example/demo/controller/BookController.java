package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.model.Category;
import com.example.demo.service.BookService;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class BookController {

    private BookService bookService;
    private CategoryService categoryService;

    @Autowired
    public BookController(BookService bookService, CategoryService categoryService) {
        this.bookService = bookService;
        this.categoryService = categoryService;
    }



    @GetMapping({"/index","/home","/"})
    public String index(Model model){
        List<Book> value = this.bookService.getAllInService();
        model.addAttribute("key",value);
        return "index";
    }
    @GetMapping("view/{id}")
    public String view(@PathVariable("id") Integer id , Model model){
        model.addAttribute("book",this.bookService.getOne(id));
        return "view";
    }
    @GetMapping("update/{id}")
    public String update(@PathVariable("id") Integer id, Model model){

        List<Category> categories = this.categoryService.getAll();

        model.addAttribute("category",categories);

        model.addAttribute("book",this.bookService.getOne(id));
        return "update";
    }
    @PostMapping("update/submit")
    public String submit(@ModelAttribute("book") Book book,MultipartFile file){

        File path = new File("c:/btb");
        if (!path.exists())
            path.mkdirs();

        String filename = file.getOriginalFilename();
        String extension= filename.substring(filename.lastIndexOf('.')+1);
//        System.out.println(extension);

        filename= UUID.randomUUID()+"."+extension;


        try {
            Files.copy(file.getInputStream(), Paths.get("c:/btb",filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(!file.isEmpty())
            book.setThumbnail("/image-btb/"+filename);
        this.bookService.updateBook(book);
        return "redirect:/index";
    }

    @GetMapping("delete/{id}")
    public String delete(@PathVariable("id")Integer id){

        this.bookService.deleteBook(id);

        return "redirect:/index";

    }
//    @GetMapping("/create")
//    public String create(Model model) {
//
//        model.addAttribute("book", new Book());
//
//        return "create";
//    }
    @PostMapping("/create/submit")
    public String create(@Valid Book book, BindingResult bindingResult, MultipartFile file) throws IOException {
        if (file== null)
        {return null;}
        File path = new File("c:/btb");
        if (!path.exists())
        path.mkdirs();

        String filename = file.getOriginalFilename();
        String extension= filename.substring(filename.lastIndexOf('.')+1);
//        System.out.println(extension);

        filename= UUID.randomUUID()+"."+extension;

        book.setThumbnail("/image-btb/"+filename);
//        System.out.println(book.getThumbnail());

        Files.copy(file.getInputStream(), Paths.get("c:/btb",filename));
        if (bindingResult.hasErrors()) {
            return "create";
        }

        this.bookService.create(book);
        return "redirect:/index";
    }



}
