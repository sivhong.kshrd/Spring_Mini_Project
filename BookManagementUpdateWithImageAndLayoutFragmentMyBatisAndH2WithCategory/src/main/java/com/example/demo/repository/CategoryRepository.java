package com.example.demo.repository;

import com.example.demo.model.Book;
import com.example.demo.model.Category;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @Select("SELECT * FROM tb_category")
    List<Category> getAll();


}
