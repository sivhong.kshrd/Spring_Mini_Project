package com.example.demo.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

    public String getAll(){
        return new SQL(){{
          SELECT("*");
          FROM("tb_book b");
          INNER_JOIN("tb_category c ON b.cate_id=c.id");


        }}.toString();
    }
    public String getOne(){
        return new SQL(){{
           SELECT("*");
           FROM("tb_book");
           WHERE("id=#{id}");
        }}.toString();
    }
    public String update(){
        return new SQL(){{
            UPDATE("tb_book");
            SET("title=#{title}");
            SET("author=#{author}");
            SET("publisher=#{publisher}");
            SET("thumbnail=#{thumbnail}");
            SET("cate_id=#{category.id}");
            WHERE("id=#{id}");

        }}.toString();
    }
}
