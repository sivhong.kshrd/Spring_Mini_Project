package com.example.demo.repository;

import com.example.demo.model.Book;
import com.example.demo.repository.provider.BookProvider;
import com.github.javafaker.Bool;
import com.github.javafaker.Faker;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface BookRepository {
//    @Select("select * from tb_book")
    @SelectProvider(type = BookProvider.class,method = "getAll")
    @Results({
            @Result(column = "id",property = "id"),
            @Result(column = "title", property = "title"),
            @Result(column = "cate_id", property = "category.id"),
            @Result(column = "name",property = "category.name")
    })
//    column bos table property bos class
    List<Book> getAllInRepository();

    @Select("select * from tb_book where id=#{id}")
    Book getOne(@Param("id") Integer id);

//    @Update("update tb_book set title=#{title},author=#{author},publisher=#{publisher},thumbnail=#{thumbnail} where id=#{id}")
    @UpdateProvider(type = BookProvider.class,method = "update")
    Boolean updateBook(Book book);

    @Delete("delete from tb_book where id= #{id}")
    Boolean deleteBook(Integer id);

    @Insert("insert into tb_book(title, author, publisher, thumbnail,cate_id) VALUES (#{title},#{author},#{publisher},#{thumbnail},#{category.id})")
    Boolean create(Book book);



//    Faker faker = new Faker();
//
//    List<Book> bookList= new ArrayList<Book>();
//    {
//        for(int i=0;i<10;i++){
//            Book book = new Book();
//            book.setId(i);
//            book.setTitle(faker.book().title());
//            book.setAuthor(faker.book().author());
//            book.setPublisher(faker.book().publisher());
//            bookList.add(book);
//
//    }
//    }
//
//    public List<Book> getAllInRepository(){
//        return this.bookList;
//    }
//    public Book getOne(Integer id){
//        for (int i = 0; i <bookList.size() ; i++) {
//            if (bookList.get(i).getId()==id)
//                return bookList.get(i);
//        }
//        return null;
//    }
//    public Boolean updateBook(Book book){
//        for (int i = 0; i <bookList.size() ; i++) {
//            if (bookList.get(i).getId()==book.getId()){
//
////                bookList.set(i,book); use set kor ban de
//
//                bookList.get(i).setTitle(book.getTitle());
//                bookList.get(i).setAuthor(book.getAuthor());
//                bookList.get(i).setPublisher(book.getPublisher());
//                bookList.get(i).setThumbnail(book.getThumbnail());
//                return true;
//            }
//        }
//
//        return false;
//    }
//    public Boolean deleteBook(Integer id){
//        for (int i = 0; i <bookList.size() ; i++) {
//            if(bookList.get(i).getId()==id)
//            {
//                bookList.remove(i);
//                return true;
//            }
//        }
//        return false;
//    }
//    public Boolean create(Book book) {
//        return bookList.add(book);
//    }

}
