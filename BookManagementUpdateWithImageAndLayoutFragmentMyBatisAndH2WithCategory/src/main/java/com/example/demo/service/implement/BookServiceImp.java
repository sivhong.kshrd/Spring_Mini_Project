package com.example.demo.service.implement;

import com.example.demo.model.Book;
import com.example.demo.repository.BookRepository;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImp implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public BookServiceImp(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    @Override
    public List<Book> getAllInService() {
        return this.bookRepository.getAllInRepository();
    }

    @Override
    public Book getOne(Integer id) {
        return this.bookRepository.getOne(id);
    }

    @Override
    public Boolean updateBook(Book book) {
        return this.bookRepository.updateBook(book);
    }

    @Override
    public Boolean deleteBook(Integer id) {
        return this.bookRepository.deleteBook(id);
    }

    @Override
    public Boolean create(Book book) {
        return this.bookRepository.create(book);
    }

}
