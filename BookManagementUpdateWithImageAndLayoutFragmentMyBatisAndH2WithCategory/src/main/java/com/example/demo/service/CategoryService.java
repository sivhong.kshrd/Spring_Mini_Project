package com.example.demo.service;

import com.example.demo.model.Book;
import com.example.demo.model.Category;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;


public interface CategoryService {
    List<Category> getAll();
}
