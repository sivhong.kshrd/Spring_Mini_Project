package com.example.demo.service;

import com.example.demo.model.Book;

import java.util.List;

public interface BookService {
    List<Book> getAllInService();
    Book getOne(Integer id);
    Boolean updateBook(Book book);
    Boolean deleteBook(Integer id);
    Boolean create(Book book);
}
