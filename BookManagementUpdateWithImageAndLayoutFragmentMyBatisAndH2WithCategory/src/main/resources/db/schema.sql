
CREATE  TABLE tb_category(
  id SERIAL PRIMARY KEY ,
  name VARCHAR (255)
 );

CREATE TABLE tb_book
(
  id        SERIAL PRIMARY KEY,
  title     VARCHAR(255),
  author    VARCHAR(255),
  publisher VARCHAR(255),
  thumbnail VARCHAR(255),
  cate_id int REFERENCES tb_category(id)
)
